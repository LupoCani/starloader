package api.listener.events.player;

import api.listener.events.Event;
import org.schema.common.util.linAlg.Vector3i;

/**
 * PlayerJoinWorldEvent.java
 * Called when a player joins a world / server.
 * Note: This event uses the player's name rather than the actual PlayerState to prevent nullpointer errors.
 *
 * @since 03/12/2021
 * @author TheDerpGamer
 */
public class PlayerJoinWorldEvent extends Event {

    private String playerName;
    private int factionId;
    private Vector3i sector;

    public PlayerJoinWorldEvent(String playerName, int factionId, Vector3i sector) {
        this.playerName = playerName;
        this.factionId = factionId;
        this.sector = sector;
    }

    /**
     * Gets the name of the player that joined.
     * @return The player's name
     */
    public String getPlayerName() {
        return playerName;
    }

    /**
     * Gets the player's faction id. Returns 0 if the player isn't in a faction.
     * @return The player's faction id
     */
    public int getFactionId() {
        return factionId;
    }

    /**
     * Gets the sector the player loaded into.
     * @return The player's sector coordinates
     */
    public Vector3i getSector() {
        return sector;
    }
}
