package api.listener.events.controller.asteroid;

import api.listener.events.Event;
import org.schema.game.common.controller.generator.AsteroidCreatorThread;
import org.schema.game.server.controller.RequestDataAsteroid;
import org.schema.game.server.controller.world.factory.WorldCreatorFloatingRockFactory;

/**
 * Called whenever the segment of an asteroid is about to be generated
 */
public class AsteroidGenerateEvent extends Event {
    private final AsteroidCreatorThread asteroidCreatorThread;
    private final RequestDataAsteroid requestData;
    private WorldCreatorFloatingRockFactory factory;

    public AsteroidGenerateEvent(AsteroidCreatorThread asteroidCreatorThread, RequestDataAsteroid requestData, WorldCreatorFloatingRockFactory factory) {
        this.asteroidCreatorThread = asteroidCreatorThread;
        this.requestData = requestData;
        this.factory = factory;

    }

    public AsteroidCreatorThread getAsteroidCreatorThread() {
        return asteroidCreatorThread;
    }

    public RequestDataAsteroid getRequestData() {
        return requestData;
    }

    public void setWorldCreatorFloatingRockFactory(WorldCreatorFloatingRockFactory factory) {
        this.factory = factory;
    }

    public WorldCreatorFloatingRockFactory getWorldCreatorFloatingRockFactory() {
        return factory;
    }
}
