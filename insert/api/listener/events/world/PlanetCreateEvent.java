package api.listener.events.world;

import api.listener.events.Event;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.Planet;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SectorInformation;
import org.schema.game.common.data.world.space.PlanetCore;

/**
 * PlanetCreateEvent.java
 * Called when a planet is first created in the galaxy.
 * ==================================================
 * Created 2/25/2021
 * @author TheDerpGamer
 */
public class PlanetCreateEvent extends Event {

    private Sector sector;
    private Planet[] planetSegments;
    private PlanetCore planetCore;
    private SectorInformation.PlanetType planetType;

    public PlanetCreateEvent(Sector sector, Planet[] planetSegments, PlanetCore planetCore, SectorInformation.PlanetType planetType) {
        this.sector = sector;
        this.planetSegments = planetSegments;
        this.planetCore = planetCore;
        this.planetType = planetType;
    }

    public Sector getSector() {
        return sector;
    }

    public Vector3i getSectorPos() {
        return getSector().pos;
    }

    public Planet[] getPlanetSegments() {
        return planetSegments;
    }

    public Planet getPlanetSegment(int i) {
        return planetSegments[i];
    }

    public PlanetCore getPlanetCore() {
        return planetCore;
    }

    public SectorInformation.PlanetType getPlanetType() {
        return planetType;
    }
}
