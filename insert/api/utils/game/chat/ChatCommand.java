package api.utils.game.chat;

import api.mod.StarMod;
import org.schema.game.common.data.player.PlayerState;
import javax.annotation.Nullable;

/**
 * @deprecated use {@link CommandInterface}
 */
@Deprecated
public abstract class ChatCommand {

    private StarMod mod;
    private String command;
    private String usage;
    private String description;
    private boolean adminOnly;

    public ChatCommand(String command, String usage, String description, boolean adminOnly, StarMod mod) {
        this.mod = mod;
        if (command.charAt(0) == '/') {
            this.command = command.substring(1);
        } else {
            this.command = command;
        }
        if (usage.charAt(0) != '/') {
            this.usage = "/" + usage;
        } else {
            this.usage = usage;
        }
        this.description = description;
        this.adminOnly = adminOnly;
    }

    public ChatCommand(String command, String usage, boolean adminOnly, StarMod mod) {
        this(command, usage, "No Description", adminOnly, mod);
    }

    public String getCommand() {
        return command;
    }

    public String getUsage() {
        return usage;
    }

    public String getDescription() {
        return description;
    }

    public boolean isAdminOnly() {
        return adminOnly;
    }

    public boolean onCommand(PlayerState sender, String[] args) {
        return true;
    }

    public void playOnServer(@Nullable PlayerState sender, String[] args){

    }

    public StarMod getMod() {
        return mod;
    }
}