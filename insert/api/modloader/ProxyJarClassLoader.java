package api.modloader;

import api.mod.ModSkeleton;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.security.ProtectionDomain;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * A ClassLoader that makes it's parent class loader define the classes.
 */
public class ProxyJarClassLoader extends URLClassLoader {

    /**
     * Gets class file from fqn. See getFQNFromClass for reverse explanation
     */
    public static String getClassFileFromFQN(String name) {
        String replace = name.replace(".", "/");
        return replace + ".class";
    }

    /**
     * Gets a full qualified name from class name
     * @param name class name, should be like `java/util/List$1.class
     * @return the full qualified name, like: `java.util.List$1
     */
    public static String getFQNFromClass(String name) {
        String replace = name.replace("/", ".");
        return replace.substring(0, replace.length() - ".class".length());
    }


    private ProxyJarClassLoader(File file) throws MalformedURLException {
        super(new URL[]{file.toURI().toURL()});
    }
    public static final ArrayList<String> modForcedLoadedClasses = new ArrayList<>();
    private static HashMap<String, byte[]> dataMap = new HashMap<>();

    /**
     * Read the jar file of a modskeleton and force define all of its classes
     * @param skeleton The ModSkeleton to load
     * @return the constructed PJCL
     */
    public static ProxyJarClassLoader construct(ModSkeleton skeleton) {
        dataMap.clear();
        System.err.println("Constructing mod from jar: " + skeleton.getJarFile().getName());
        try {
            ProxyJarClassLoader loader = new ProxyJarClassLoader(skeleton.getJarFile());
            //Has to exist, otherwise it wouldn't be able to even start
            ZipInputStream zip = new ZipInputStream(new FileInputStream(skeleton.getJarFile()));
            while (true) {
                ZipEntry e = zip.getNextEntry();
                if (e == null) {
                    System.out.println("Reached end of jar");
                    break;
                }
                String name = e.getName();
                if (!name.startsWith("org/schema") || skeleton.isHardLoadAllClasses()) {
                    if (name.endsWith(".class")) {
                        dataMap.put(getFQNFromClass(name), IOUtils.toByteArray(zip));
                        modForcedLoadedClasses.add(name);
                    }
                }
            }
            for (Map.Entry<String, byte[]> entry : dataMap.entrySet()) {
                //String name, byte[] b, int off, int len, ProtectionDomain protectionDomain
                hardDefineClass(entry.getKey(), entry.getValue());
            }
            dataMap.clear();
            return loader;
        } catch (IOException | NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }

        throw new RuntimeException("Failed to construct classloader");
    }

    /**
     * Loads a class on the parent classloader
     * @param name Full qualified name
     * @param classBytes bytecode of the class
     */
    public static void hardDefineClass(String name, byte[] classBytes) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        System.err.println("> Hard loading mod class: " + name);
        int majorVersion = classBytes[6] << 8 | classBytes[7]; //Structure of class [0xCA FE BA BE, 2 bytes for minor, 2 bytes for major]
        if(majorVersion != 51){
            //https://stackoverflow.com/questions/9170832/list-of-java-class-file-format-major-version-numbers
            //51 = Java 7
//            throw new IrregularClassVersionError(name, majorVersion);
        }
        Method m = ClassLoader.class.getDeclaredMethod("defineClass", String.class, byte[].class, int.class, int.class, ProtectionDomain.class);
        m.setAccessible(true);
        try {
            m.invoke(ProxyJarClassLoader.class.getClassLoader(), name, classBytes, 0, classBytes.length, null);
        }catch (InvocationTargetException e){
            //FIXME Pretty bad fix for classes being loaded in the wrong order, Sort these before enable instead
            if(e.getCause() instanceof NoClassDefFoundError){
                NoClassDefFoundError cause = (NoClassDefFoundError) e.getCause();
                String cName = cause.getMessage().replace("/", ".");
                System.err.println("Need to load: " + cName + " before " + name);
                hardDefineClass(cName, dataMap.get(cName));
                //Retry to load our class after the dependency is loaded.
                hardDefineClass(name, classBytes);

            }
        }
    }
}