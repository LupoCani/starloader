package api.modloader;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.instrument.Instrumentation;
import java.lang.instrument.UnmodifiableClassException;
import java.util.HashMap;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

/**
 * Created by Jake on 10/8/2020.
 * <insert description here>
 */
public class StarAgent {
    public static Instrumentation instrumentation;
    public static void commonmain(String agentArgs, Instrumentation inst){
        System.err.println("== StarLoader instrumentation agent begin ==");
        instrumentation = inst;
        getStarLoaderClasses();
        StarClassTransformer dt = new StarClassTransformer();
        inst.addTransformer(dt, true);
        System.err.println("== StarLoader instrumentation agent end ==");
    }
    public static void agentmain(String agentArgs, Instrumentation inst) {
        commonmain(agentArgs, inst);
    }
    public static void premain(String agentArgs, Instrumentation inst) {
        commonmain(agentArgs, inst);
    }
    public static void registerCustomClass(String className, byte[] byteCode){
        modClassBytes.put(className, byteCode);
    }
    public static void retransform(Class<?> toRetransform){
        try {
            instrumentation.retransformClasses(toRetransform);
        } catch (UnmodifiableClassException e) {
            e.printStackTrace();
        }
    }

    private static HashMap<String, byte[]> starloaderClassBytes = null;
    private static HashMap<String, byte[]> modClassBytes = new HashMap<>();

    public static void clearModClasses(){
        modClassBytes.clear();
    }

    public static HashMap<String, byte[]> getModClassBytes() {
        return modClassBytes;
    }

    public static HashMap<String, byte[]> getStarLoaderClasses() {
        if (starloaderClassBytes != null) {
            return starloaderClassBytes;
        }
        starloaderClassBytes = new HashMap<>();
        try {
            //Has to exist, otherwise it wouldn't be able to even start
            ZipFile agentZip = new ZipFile("starloader.jar");
            File agentJar = new File("starloader.jar");
            ZipInputStream zip = new ZipInputStream(new FileInputStream(agentJar));
            while (true) {
                ZipEntry e = zip.getNextEntry();
                if (e == null) {
                    System.out.println("Reached end of jar");
                    break;
                }
                String name = e.getName();
                if (name.startsWith("org") || name.startsWith("api")) {
                    if(name.endsWith(".class")) {
                        InputStream res = agentZip.getInputStream(e);
//                        InputStream res = Thread.currentThread().getContextClassLoader().getResourceAsStream(name);
                        byte[] buffer = IOUtils.toByteArray(res);
                        starloaderClassBytes.put(name, buffer);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return starloaderClassBytes;
    }
    public static byte[] getClassFromJar(JarFile jarFile, String fullName){
        try {
            String entryName = fullName.replace(".", "/") + ".class";
            ZipEntry entry = jarFile.getEntry(entryName);
            InputStream inputStream = jarFile.getInputStream(entry);
            return IOUtils.toByteArray(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
