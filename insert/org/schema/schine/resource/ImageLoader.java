//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package org.schema.schine.resource;

import api.listener.events.draw.SpriteLoadEvent;
import api.mod.StarLoader;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.ImageProbs;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.graphicsengine.texture.Texture;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ImageLoader {
    static Pattern multiTexturePathPattern = Pattern.compile("(.*)-([0-9]+x[0-9]+)(.*)");
    private final Object2ObjectOpenHashMap<String, ImageProbs> imageMap = new Object2ObjectOpenHashMap();
    private final Object2ObjectOpenHashMap<String, Sprite> spriteMap = new Object2ObjectOpenHashMap();

    public ImageLoader() {
    }

    public Object2ObjectOpenHashMap<String, ImageProbs> getImageMap() {
        return this.imageMap;
    }

    public Object2ObjectOpenHashMap<String, Sprite> getSpriteMap() {
        return this.spriteMap;
    }

    public void loadImage(String var1, String var2) throws IOException {
        this.loadImage(var1, var2, false);
    }

    public void loadImage(String path, String name, boolean var3) throws IOException {
        long var5 = System.currentTimeMillis();
        Texture texture = Controller.getTexLoader().getTexture2D(path, !name.contains("-gui-"), !var3 && !name.contains("-gui-"));
        System.currentTimeMillis();
        Sprite sprite;
        (sprite = new Sprite(texture)).setName(name);
        sprite.setPositionCenter(name.contains("-c-"));
        Matcher var7;
        if ((var7 = multiTexturePathPattern.matcher(path)).matches()) {
            String[] var8;
            int var9 = Integer.parseInt((var8 = var7.group(2).split("x"))[0]);
            int var11 = Integer.parseInt(var8[1]);
            sprite.setMultiSpriteMax(var9, var11);
            sprite.setWidth(texture.getWidth() / var9);
            sprite.setHeight(texture.getHeight() / var11);
        }

        sprite.onInit();
        //INSERTED CODE
        SpriteLoadEvent event = new SpriteLoadEvent(path, name, texture, sprite);
        StarLoader.fireEvent(event, false);
        ///
        this.getSpriteMap().put(name, sprite);
        long var12;
        if ((var12 = System.currentTimeMillis() - var5) > 300L) {
            System.err.println("[WARNING] initializing Texture " + path + " took " + var12 + " ms");
        }

    }
}
