package org.schema.game.common.data.chat.prefixprocessors;

import api.mod.StarLoader;
import api.network.packets.PacketUtil;
import api.network.packets.client.PacketCSAdminCommand;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.GameClientController;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.chat.ChatChannel;
import org.schema.game.network.objects.ChatMessage;
import org.schema.game.server.data.admin.AdminCommandIllegalArgument;
import org.schema.game.server.data.admin.AdminCommands;
import java.util.Locale;

public class PrefixProcessorAdminCommand extends AbstractPrefixProcessor {
    public PrefixProcessorAdminCommand() {
        super("/");
    }

    protected void process(ChatMessage var1, String var2, String var3, ChatChannel var4, GameClientState var5) {
        //INSERTED CODE @22
        if(processCustomCommand(var2, var3)) return;
        //
        try {
            try {
                AdminCommands var10 = (AdminCommands)Enum.valueOf(AdminCommands.class, var2.toUpperCase(Locale.ENGLISH));
                if ((var3 = var3.trim()).length() > 0) {
                    String[] var11 = StringTools.splitParameters(var3);
                    Object[] var12 = AdminCommands.packParameters(var10, var11);
                    if (var10.isLocalCommand()) {
                        var10.processLocal(var4, var5, var12);
                    } else {
                        var5.getController().sendAdminCommand(var10, var12);
                    }
                } else if (var10.getTotalParameterCount() > 0) {
                    var3 = "need ";
                    if (var10.getRequiredParameterCount() != var10.getTotalParameterCount()) {
                        var3 = var3 + "minimum of " + var10.getRequiredParameterCount();
                    } else {
                        var3 = var3 + var10.getTotalParameterCount();
                    }

                    throw new AdminCommandIllegalArgument(var10, (String[])null, "No parameters provided: " + var3);
                } else if (var10.isLocalCommand()) {
                    var10.processLocal(var4, var5, new Object[0]);
                } else {
                    var5.getController().sendAdminCommand(var10, new Object[0]);
                }
            } catch (IllegalArgumentException var6) {
                throw new IllegalArgumentException(GameClientController.findCorrectedCommand(var2));
            }
        } catch (IllegalArgumentException var7) {
            if (!var7.getMessage().startsWith("[ERROR]")) {
                this.localResponse("[ERROR] UNKNOWN COMMAND: " + var2, var4);
            } else {
                this.localResponse(var7.getMessage(), var4);
            }
        } catch (IndexOutOfBoundsException var8) {
            this.localResponse(var8.getMessage(), var4);
        } catch (AdminCommandIllegalArgument var9) {
            if (var9.getMsg() != null) {
                this.localResponse("[ERROR] " + var9.getCommand() + ": " + var9.getMsg(), var4);
                this.localResponse("[ERROR] usage: " + var9.getCommand().getDescription(), var4);
            } else {
                this.localResponse(var9.getMessage(), var4);
            }
        }
    }

    //INSERTED CODE @74

    public static String[] createArgsFromString(String rawArgs) {
        return rawArgs.split(" ");
    }

    private boolean processCustomCommand(String commandUsed, String argsString) {
        if(StarLoader.getClientCommandList().contains(commandUsed)) {
            String[] args = createArgsFromString(argsString);
            PacketCSAdminCommand cmd = new PacketCSAdminCommand(commandUsed, args);
            PacketUtil.sendPacketToServer(cmd);
            return true;
        } else return false;
        //PlayerUtils.sendMessage(GameClient.getClientPlayerState(), commandUsed + " is not a valid command.");
        //PlayerCustomCommandEvent commandEvent = new PlayerCustomCommandEvent(command,commandUsed + " " + argsString, args, state.getPlayer());
        //boolean success = command.onCommand(state.getPlayer(), args);
        //StarLoader.fireEvent(commandEvent, true);

        //if(!success) PlayerUtils.sendMessage(state.getPlayer(), "[ERROR]: Incorrect usage \"" + commandUsed + " " + argsString + "\"\nUse /help " + commandUsed + " for proper usages.");
    }
    //

    public boolean sendChatMessageAfterProcessing() {
        return false;
    }
}