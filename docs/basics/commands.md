Commands
========
###Creating a new Command
All custom commands must extend the ChatCommand class and override the `onCommand()` method with their own functionality.

Example:

```
 import api.utils.game.chat.ChatCommand;

 public class ExampleCommand extends ChatCommand {
 
     public ExampleCommand() {
         super("examplecommand", "/examplecommand", "Example Command", false);
         //super("<commandname>", "<usage>", "<description>", <isadminonly>);
     }
 
     @Override
     public boolean onCommand(PlayerState sender, String[] args) {
         PlayerUtils.sendMessage(sender, "This is an example command");
         return true; //Return false only if the player typed the command or it's arguments incorrectly
     }
 }
```
##Registering a new Command
To register a new command, call the `StarLoader.registerCommand()` method in your mod's `onGameStart()` method like so:
```
@Override
public void onGameStart() {
...
    StarLoader.registerCommand(new ExampleCommand());
} 

```