Introduction
============================
### StarLoader vs StarAPI
StarMade modding is actually broken up into two separate projects - StarLoader and StarAPI.
StarLoader handles the loading and installment of mods, as well as mod event listeners. It contains
a few helper classes, but for the most part the API itself is part of the StarAPI project. StarAPI
is actually optional, and is not required for modding, but it's usage is highly recommended as it
contains wrappers and helper classes in order to make modding StarMade easier. This documentation
assumes you are using StarAPI in your project as a dependency and have it in your /mods folder.
### Links
[StarLoader Repository](https://gitlab.com/generic-username/starloader)

[StarAPI Repository](https://gitlab.com/generic-username/star-api)

[JavaDocs](https://generic-username.gitlab.io/starloader/)

[Discord Server](https://discord.gg/Ekr8BaX)