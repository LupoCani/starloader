About
=====
###StarLoader:
Todo
###Contributing:
#####API
Todo
#####Documentation
If you have contribution access, writing documentation is easy.All documentation is stored in the `/docs` folder in the
repository's root directory. Simple make a new section or create a new file in the appropriate section in the
documentation for your guide. Then, you must add the page to the nav configuration in `mkdocs.yml`, which is located
in the root directory.

Example:

```
nav:
...
  - Example Section: example/examplepage.md
```~~~~